    var app = new Vue({
        el:"#LemonCD",
        data:{
            lemons:[
                {
                    id: 1,
                    title: "TAG 2000",
                    short_text: "Information about TAG 2000. Very important!",
                    img: 'TAG 2000.png',
                    desc: "Full desc"
                },
                {
                    id: 2,
                    title: "TAG 2001",
                    short_text: "Information about TAG 2001. Very important!",
                    img: 'TAG 2001.png',
                    desc: "Full desc"
                },
                {
                    id: 3,
                    title: "TAG 2002",
                    short_text: "Information about TAG 2002. Very important!",
                    img: 'TAG 2002.png',
                    desc: "Full desc"
                },
                {
                    id: 4,
                    title: "TAG 2003",
                    short_text: "Information about TAG 2003. Very important!",
                    img: 'TAG 2003.png',
                    desc: "Full desc"
                },
                {
                    id: 5,
                    title: "TAG 2004",
                    short_text: "Information about TAG 2004. Very important!",
                    img: 'TAG 2004.png',
                    desc: "Full desc"
                }                
            ],
            lemon:[],
            carts:[],
            contactFields:[],
            btnVisible: 0,
            message: 'test work'
        },
        mounted:function(){
            this.getLemon();
            this.checkInCarts();
            this.getCart();
        },
        methods:{            
            addItem:function(id){
                window.localStorage.setItem('prod',id);
            },
            getLemon:function(){
                if(window.location.hash){
                    var id = window.location.hash.replace('#','');
                    if (this.lemons && this.lemons.length > 0){
                        for (i in this.lemons){
                            if(this.lemons[i] && this.lemons[i].id && id==this.lemons[i].id){
                                this.lemon=this.lemons[i];
                            }
                        }
                    }
                }
            },
            addToCarts:function(id){
                console.log(id)
                var carts = [];
                if (window.localStorage.getItem('carts')){
                    carts = window.localStorage.getItem('carts').split(',');
                }

                if(carts.indexOf(String(id))==-1){
                    carts.push(id);
                    window.localStorage.setItem('carts',carts.join());
                    this.btnVisible=1;
                }
            },
            checkInCarts:function(){
                if(this.lemon && this.lemon.id && window.localStorage.getItem('carts').split(',').indexOf(String(this.lemon.id))!=-1) this.btnVisible=1;
            },
            getCart:function(){
                var storage = [];
                storage = localStorage.getItem('carts').split(',');
                for (i in this.lemons) {
                    if (storage.indexOf(String(this.lemons[i].id)) != -1) {
                        this.carts.push(this.lemons[i]);
                    }
                }
                
            },
            RemoveFromCarts:function(id){
                var storage = [];
                storage = window.localStorage.getItem('carts').split(',');
                console.log(storage);
                storage = storage.filter(storageId => storageId != id);
                window.localStorage.setItem('carts', storage.join());                
                this.carts = this.carts.filter(item => item.id != id);        
                console.log(storage);
                console.log(id);
                
            },
            makeOrder: function () {
                localStorage.clear();
                this.carts.splice(0)
                this.formVisible = 0                
            },    
        }
    });